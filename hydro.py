import numpy as np
from matplotlib import pyplot as plt

class Hydro:
    def __init__(self):
        pass

    @staticmethod
    def calculate_flow(liq_pos, pressure, density, u, t_fin, gamma=1.4, geometry='planar', t_array=np.array([-1.0]),
                       u_bound=None, args_u_bound=()):
        if t_array[0] == -1.0:
            t_array[0] = t_fin
            # print(f't_fin = {t_fin}')

        pi = np.pi

        if geometry == 'spherical':
            coeff_volume = 4.0 / 3.0 * pi
            coeff_area = 4.0 * pi
            coeff_pow = 1.0 / 3.0
            dim = 3.0

        if geometry == 'planar':
            coeff_volume = 1.0
            coeff_area = 1.0
            coeff_pow = 1
            dim = 1.

        if geometry == 'cylindrical':
            coeff_area = 2.0 * pi
            coeff_volume = pi
            coeff_pow = (1.0 / 2.0)
            dim = 2.

        a = np.zeros(len(u))

        energy = (1.0 / (gamma - 1)) * pressure / density
        time_current = 0
        liq_pos_avg = ((liq_pos[1:] + liq_pos[:-1]) * 0.5)
        visc = calculate_viscocity(u, density)
        V = coeff_volume * np.diff(liq_pos ** dim)
        mass = V * density

        number_of_iterations = 0

        results_liq_pos = np.ndarray([len(t_array), len(liq_pos)])
        results_u = np.empty_like(results_liq_pos)
        results_pressure = np.ndarray([len(t_array), len(liq_pos) - 1])
        results_density = np.empty_like(results_pressure)
        results_energy = np.empty_like(results_density)
        results_visc = np.empty_like(results_energy)
        results_liq_pos_avg = np.empty_like(results_visc)
        results_V = np.empty_like(results_liq_pos_avg)

        index_t_check = 0

        while time_current <= t_fin:

            if number_of_iterations % 50 == 0:
                pass
                # print(time_current)
#                 plt.plot(liq_pos, u)
#                 plt.grid()
#                 plt.title(time_current)
#                 plt.show()
#                 plt.close()

            dt = find_dt(liq_pos, pressure, density, u, gamma)

            if np.abs(time_current - t_array[index_t_check]) <= dt:
                results_liq_pos[index_t_check, :] = liq_pos
                results_density[index_t_check, :] = density
                results_pressure[index_t_check, :] = pressure
                results_energy[index_t_check, :] = energy
                results_u[index_t_check, :] = u
                results_visc[index_t_check, :] = visc
                results_liq_pos_avg[index_t_check, :] = liq_pos_avg
                results_V[index_t_check, :] = V

                index_t_check += 1
                if index_t_check == len(t_array):
                    index_t_check -= 1

            # 1
            if u_bound != None:
                u[-1] = u_bound(liq_pos[-1], time_current + dt / 2., *args_u_bound)
            u_np = u + (dt / 2.) * a  # u_np is the speed at time n+1/2
            # print(f'u_np = {u_np}')
            # 2
            liq_pos = liq_pos + dt * u_np
            # print(f'liq_pos = {liq_pos}')
            # 3
            V_n1 = coeff_volume * np.diff(liq_pos ** dim)  # V_n1 is the volume at time n+1
            # print(f'V_n1 = {V_n1}')
            # 4
            density = mass / V_n1
            # print(f'density = {density}')
            # 4.5
            visc_n1 = calculate_viscocity(u_np, density)
            # 5
            liq_pos_avg = (liq_pos[1:] + liq_pos[:-1]) / 2.
            coeff_pres_two = coeff_area * liq_pos ** (dim - 1)

            energy = (energy - 0.5 * (pressure + (visc_n1 + visc)) * (V_n1 - V) / mass) / (
                    1 + 0.5 * (gamma - 1) * (V_n1 - V) * density / mass)
            # 6
            pressure = (gamma - 1) * energy * density
            # 7
            a[1:-1] = -2.0 * (
                    (coeff_pres_two[1:-1] * np.diff(pressure + visc_n1)) / (mass[1:] + mass[:-1]))
            a[-1] = a[0] = 0
            # 8
            u = u_np + (dt / 2.0) * a

            visc = visc_n1
            V = V_n1

            time_current += dt

            number_of_iterations += 1

        return results_liq_pos, results_pressure, results_density, results_energy, results_u, results_visc, results_liq_pos_avg, V, t_array

    @staticmethod
    def find_shock(x, visc):
        index_max = np.argmax(visc)
        if index_max == 0:
            return x[0]
        if index_max == len(visc) - 1:
            return x[index_max]
        parab = np.polyfit(x[index_max - 1:index_max + 2], visc[index_max - 1:index_max + 2], 2)
        x_maximum = -parab[1] / (2.0 * parab[0])
        return x_maximum


def find_dt(liq_pos, pressure, density, u, gamma):
    dx_array = np.diff(liq_pos)

    sound_speed = np.sqrt(np.abs(gamma * pressure / density))
    if not all(sound_speed == 0):
        not_zero = sound_speed != 0.0
        supermum = np.amin(0.2 * dx_array[not_zero] / sound_speed[not_zero])
        return supermum 

    u_avg = (u[1:] + u[:-1]) * 0.5
    dist_vel = np.max(np.abs([u_avg + sound_speed, u_avg - sound_speed]))
    supermum = np.amin(0.2 * dx_array / dist_vel)
    return supermum 


def calculate_viscocity(u, density, sigma=2.0):
    diff_u = np.diff(u)
    new_visc = np.ndarray(len(diff_u))
    new_visc[diff_u >= 0.] = 0.
    new_visc[diff_u < 0.] = (sigma * density * diff_u ** 2)[diff_u < 0.]

    return new_visc


def u_bound(r, t, t_init, lam, f_interp):
    x = (t + t_init)/ r ** lam
    u = - r / ((t + t_init) * lam) * f_interp(x)
    return u  
